package com.microservice.currencyexc.allclasss;

import java.math.BigDecimal;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Controller {
	@Autowired
	private CurrencyExchangeProxy proxy;

	@GetMapping("/currency-conversion/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversion calculateCurrencyConversion(@PathVariable String from,@PathVariable String to, @PathVariable BigDecimal quantity) {
		HashMap<String, String> uriVariables=new HashMap<>();
		uriVariables.put("from", from);
		uriVariables.put("to", to);
		ResponseEntity<CurrencyConversion>response= new RestTemplate().getForEntity("http://localhost:8000/currency-exchange/from/{from}/to/{to}", CurrencyConversion.class,uriVariables);
		CurrencyConversion conversion=response.getBody();
		return new CurrencyConversion(conversion.getId(),from,to,conversion.getConversionMultiple(),quantity,quantity.multiply(conversion.getConversionMultiple()), conversion.getEnvironment());
	}
	
	@GetMapping("/currency-conversion-feign/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversion usingFeignClient(@PathVariable String from,@PathVariable String to, @PathVariable BigDecimal quantity) {
		CurrencyConversion conversion=proxy.retrieveExchangedValue(from, to);
		return new CurrencyConversion(conversion.getId(),from,to,conversion.getConversionMultiple(),quantity,quantity.multiply(conversion.getConversionMultiple()), conversion.getEnvironment());
		
	}
}
